

**Nom du Microservice** : Gestion des utilisateurs OwnCloud

**Description** :
Le microservice "Gestion des utilisateurs OwnCloud" est une application Flask qui facilite la gestion des utilisateurs sur un serveur OwnCloud. Il offre un ensemble de fonctionnalités permettant d'ajouter, de mettre à jour et de supprimer des utilisateurs sur un serveur OwnCloud en utilisant des requêtes HTTP.

**Fonctionnalités principales** :
1. **Ajout d'utilisateur** : Permet d'ajouter un nouvel utilisateur à votre serveur OwnCloud en spécifiant un nom d'utilisateur (userid) et un mot de passe. Vous avez également la possibilité d'attribuer l'utilisateur à un ou plusieurs groupes.

2. **Mise à jour de l'utilisateur** : Vous pouvez mettre à jour le mot de passe d'un utilisateur existant en spécifiant son nom d'utilisateur (username) et le nouveau mot de passe (newpassword).

3. **Suppression d'utilisateur** : Permet de supprimer un utilisateur existant en spécifiant son nom d'utilisateur (username).

**Authentification** :
Le microservice utilise l'authentification de base (Basic Authentication) pour s'authentifier sur le serveur OwnCloud. Vous devez fournir des identifiants d'administrateur dans le fichier `.env` pour garantir les droits d'accès nécessaires à la gestion des utilisateurs.

**Gestion des erreurs** :
Le microservice prend en charge la gestion des erreurs et renvoie des réponses appropriées en cas de succès ou d'échec des opérations. Il utilise des codes de statut HTTP tels que 200 (OK), 400 (Bad Request) et 500 (Internal Server Error) pour indiquer le résultat de l'opération.

**Sécurité** :
Le microservice assure la sécurité des données sensibles en transmettant les informations d'identification et les données d'utilisateur via des connexions HTTPS sécurisées.

**Technologies utilisées** :
- Flask : Un framework web Python pour la gestion des requêtes HTTP.
- Requests : Une bibliothèque Python pour effectuer des requêtes HTTP vers le serveur OwnCloud.
- HTML et CSS : Pour la création de pages web conviviales.
- Basic Authentication : Pour l'authentification du microservice sur le serveur OwnCloud.

**Utilisation** :
Ce microservice est destiné à être utilisé par des administrateurs système ou des gestionnaires de serveurs OwnCloud. Il simplifie les tâches de gestion des utilisateurs en automatisant les opérations d'ajout, de mise à jour et de suppression, ce qui peut être particulièrement utile dans un environnement de serveur OwnCloud en évolution.

Le microservice peut être déployé sur un serveur Web et interagit avec l'API OwnCloud pour effectuer les opérations nécessaires. Pour l'utiliser, les utilisateurs doivent accéder aux pages web associées aux fonctionnalités d'ajout, de mise à jour et de suppression.

En résumé, le microservice "Gestion des utilisateurs OwnCloud" offre une solution simple et automatisée pour gérer les utilisateurs sur un serveur OwnCloud, améliorant ainsi l'efficacité et la facilité de gestion de ce service de stockage en ligne.


## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
